# DBM Yelp Project

## Group Members
- Asare Buahin
- Matt Dionne
- Raffa Nimir
- Alaysha Shearn

## Project Description
For our project we will convert the [Yelp reviews dataset](https://www.kaggle.com/datasets/yelp-dataset/yelp-dataset?select=yelp_academic_dataset_business.json) into a normalized SQL database. Our SQL database will have tables that include business information, user information, reviews, tips and more. Storing the data as an SQL database will make it easier to explore relationships between the data in each table. 

Once the SQL database has been set up, we plan to attempt two types of data analysis. First, we will create a business dashboard that can provide a business owner with useful information about their business and nearby competitors. This includes plots of how their star ratings have changed over time, plots of check in counts for nearby businesses (competitors), and a summary of the most commonly used words in their reviews. Next, we conduct sentiment analysis of the reviews to assess the tone of the reviews and to give a list of pros and cons and recommended improvements to the busienss.

## Files
- sql_conversion.ipynb: Jupyter notebook to be used to convert the data into a MySQL database
- business_dashboard.ipynb: Jupyter notebook that has the interactive business dashboard
- SQL_Project_Sentiment_Analysis.ipynb: Jupyter notebook used to conduct sentiment analysis
